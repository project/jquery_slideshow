Drupal.behaviors.jQuerySlideshow = function(context) {
  if (Drupal.settings.jqueryslideshow) {
    jQuery.each(Drupal.settings.jqueryslideshow, function(id, options) {
      $('#' + id + ':not(".jqueryslideshow-processed")', context)
        .addClass('jqueryslideshow-processed')
        .css({height: options.height + 'px', width: options.width + 'px'})
        .cycle(options);
    });
  }
};